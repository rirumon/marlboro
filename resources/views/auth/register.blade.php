@extends('auth.app')
@section('title') @translate(Register) @endsection

@section('content')

    <div class="card auth-box-shadow">
        <div class="card-header text-center fs-32 border-0">@translate(Register)</div>

        <div class="card-body">
            <form method="POST" action="{{ route('register') }}">
                @csrf

                <div class="form-group">
                    <label for="name" class=" col-form-label text-md-right">@translate(Name)</label>

                    <div class="">
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                               name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                        @error('name')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group ">
                    <label for="email" class="col-form-label text-md-right">@translate(E-Mail Address)</label>

                    <div class="">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                               name="email" value="{{ old('email') }}" required autocomplete="email">

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group">
                    <label for="password" class="col-form-label text-md-right">@translate(Password)</label>

                    <div class="">
                        <input id="password" type="password"
                               class="form-control @error('password') is-invalid @enderror" name="password" required
                               autocomplete="new-password">

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group ">
                    <label for="password-confirm" class=" col-form-label text-md-right">@translate(Confirm
                        Password)</label>

                    <div class="">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                               required autocomplete="new-password">
                    </div>
                </div>

                <div class="form-group">

                    <button type="submit" class="btn btn-outline-primary btn-block">
                        @translate(Register)
                    </button>

                </div>
            </form>

            <div class="row p-2">
                <div class="col-md-4">
                    <a href="#" class="btn btn-outline-dark w-100">Google</a>
                </div>
                <div class="col-md-4">
                    <a href="#" class="btn btn-outline-dark w-100">Facebook</a>
                </div>
                <div class="col-md-4">
                    <a href="#" class="btn btn-outline-dark w-100">Github</a>
                </div>
            </div>

            <div class="row p-2">

                <div class="float-left">
                    @if (Route::has('login'))
                        <a class="btn btn-link text-righ" href="{{ route('login') }}">
                            @translate(Login)
                        </a>
                    @endif
                </div>

            </div>
        </div>
    </div>


@endsection
