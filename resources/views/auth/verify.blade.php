@extends('auth.app')
@section('title') @translate(Verify) @endsection

@section('content')
            <div class="card auth-box-shadow">
                <div class="card-header text-center fs-32 border-0">@translate(Verify Your Email Address)</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            @translate(A fresh verification link has been sent to your email address).
                        </div>
                    @endif

                    @translate(Before proceeding, please check your email for a verification link).
                    @translate(If you did not receive the email),
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-block btn-link p-0 m-0 align-baseline">@translate(click here to request another)</button>.
                    </form>
                </div>
            </div>

@endsection
