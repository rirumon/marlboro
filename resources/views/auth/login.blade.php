@extends('auth.app')
@section('title') @translate(Login) @endsection

@section('content')
    <div class="card auth-box-shadow">
        <h2 class="card-header text-center fs-32 border-0">@translate(Login)</h2>

        <div class="card-body">
            <form method="POST" action="{{ route('login') }}">
                @csrf

                <div class="form-group ">
                    <label for="email" class="col-form-label text-md-right">@translate(E-Mail Address)</label>


                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                           name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                    @error('email')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror

                </div>

                <div class="form-group ">
                    <label for="password" class="col-form-label text-md-right">@translate(Password)</label>


                    <input id="password" type="password"
                           class="form-control @error('password') is-invalid @enderror" name="password" required
                           autocomplete="current-password">

                    @error('password')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror

                </div>

                <div class="form-group ">

                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="remember"
                               id="remember" {{ old('remember') ? 'checked' : '' }}>

                        <label class="form-check-label" for="remember">
                            @translate(Remember Me)
                        </label>
                    </div>

                </div>

                <div class="form-group  mb-0">

                    <button type="submit" class="btn btn-outline-primary btn-block">
                        @translate(Login)
                    </button>


                </div>
            </form>
        </div>

        <div class="row p-2">
            <div class="col-md-4">
                <a href="#" class="btn btn-outline-dark w-100">Google</a>
            </div>
            <div class="col-md-4">
                <a href="#" class="btn btn-outline-dark w-100">Facebook</a>
            </div>
            <div class="col-md-4">
                <a href="#" class="btn btn-outline-dark w-100">Github</a>
            </div>
        </div>

        <div class="row p-2">
            <div class="float-left">
                @if (Route::has('password.request'))
                    <a class="btn btn-link" href="{{ route('password.request') }}">
                        @translate(Forgot Your Password)
                    </a>
                @endif
            </div>

            <div class="float-right">
                @if (Route::has('register'))
                    <a class="btn btn-link text-righ" href="{{ route('register') }}">
                        @translate(Register)
                    </a>
                @endif
            </div>

        </div>


    </div>
@endsection
