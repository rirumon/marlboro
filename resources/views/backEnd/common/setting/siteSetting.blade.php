@extends('backEnd.layouts.master')
@section('title') @translate(Setting)  @endsection
@section('content')
    <div class="row">
        <div class="col-md-8 offset-2">
            <div class="card m-2">
                <div class="card-header">
                    <h2 class="card-title">@translate(Setup Organization Setting)</h2>
                </div>
                <div class="card-body">
                    <form method="post" action="{{route('site.update')}}" enctype="multipart/form-data">
                    @csrf

                    <!--logo-->
                        <label class="label">@translate(Organization logo)</label>
                        <input type="hidden" value="type_logo" name="type_logo">

                        <div class="avatar-upload">
                            <div class="avatar-edit">
                                <input type='file' name="logo" id="imageUpload" accept=".png, .jpg, .jpeg"/>
                                <label for="imageUpload"></label>
                            </div>
                            <div class="avatar-preview">
                                <div id="imagePreview"
                                     style="background-image: url({{filePath(getSystemSetting('type_logo'))}});">
                                </div>
                            </div>
                        </div>
                        <!--logo end-->

                        <!--footer logo-->
                        <label class="label">@translate(Footer Logo)</label>
                        <input type="hidden" value="footer_logo" name="footer_logo">

                        <div class="avatar-upload">
                            <div class="avatar-edit">
                                <input type='file' name="f_logo" id="imageUpload_f_logo" accept=".png, .jpg, .jpeg"/>
                                <label for="imageUpload_f_logo"></label>
                            </div>
                            <div class="avatar-preview">
                                <div id="imagePreview_f_logo"
                                     style="background-image: url({{filePath(getSystemSetting('footer_logo'))}});">
                                </div>
                            </div>
                        </div>
                        <!--footer logo end-->

                        <!--favicon icon-->
                        <label class="label">@translate(Favicon Icon)</label>
                        <input type="hidden" value="favicon_icon" name="favicon_icon">


                        <div class="avatar-upload">
                            <div class="avatar-edit">
                                <input type='file' name="f_icon" id="imageUpload_f_icon" accept=".png, .jpg, .jpeg"/>
                                <label for="imageUpload_f_icon"></label>
                            </div>
                            <div class="avatar-preview">
                                <div id="imagePreview_f_icon"
                                     style="background-image: url({{filePath(getSystemSetting('favicon_icon'))}}">
                                </div>
                            </div>
                        </div>
                        <!--favicon end-->

                        <!--name-->
                        <label class="label">@translate(Organization Name)</label>
                        <input type="hidden" value="type_name" name="type_name">
                        <input type="text" value="{{getSystemSetting('type_name')}}" name="name"
                               class="form-control">

                        <!--footer-->
                        <label class="label">@translate(Organization Footer)</label>
                        <input type="hidden" value="type_footer" name="type_footer">
                        <input type="text" value="{{getSystemSetting('type_footer')}}" name="footer"
                               class="form-control">

                        <!--address-->
                        <label class="label">@translate(Address)</label>
                        <input type="hidden" value="type_address" name="type_address">
                        <input type="text" value="{{getSystemSetting('type_address')}}" name="address"
                               class="form-control">

                        <!--mail-->
                        <label class="label">@translate(Organization Mail)</label>
                        <input type="hidden" value="type_mail" name="type_mail">
                        <input type="text" value="{{getSystemSetting('type_mail')}}" name="mail"
                               class="form-control">

                        <!--fb-->
                        <label class="label">@translate(Organization Facebook Link)</label>
                        <input type="hidden" value="type_fb" name="type_fb">
                        <input type="text" value="{{getSystemSetting('type_fb')}}" name="fb"
                               class="form-control">

                        <!--tw-->
                        <label class="label">@translate(Organization Twitter Link)</label>
                        <input type="hidden" value="type_tw" name="type_tw">
                        <input type="text" value="{{getSystemSetting('type_tw')}}" name="tw"
                               class="form-control">

                        <!--google-->
                        <label class="label">@translate(Organization Google Link)</label>
                        <input type="hidden" value="type_google" name="type_google">
                        <input type="text" value="{{getSystemSetting('type_google')}}" name="google"
                               class="form-control">

                        <!--Number-->
                        <label class="label">@translate(Organization Number )</label>
                        <input type="hidden" value="type_number" name="type_number">
                        <input type="text" value="{{getSystemSetting('type_number')}}" name="number"
                               class="form-control">

                        <div class="m-2 float-right">
                            <button class="btn btn-block btn-primary" type="submit">@translate(Save)</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>


@endsection



@section('js-link')

@stop

@section('page-script')
@stop
