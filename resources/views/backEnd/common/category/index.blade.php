@extends('backEnd.layouts.master')
@section('title') @translate(Categories)
@endsection
@section('parentPageTitle', 'All Category')
@section('content')
    <div class="card m-2">
        <div class="card-header">
            <div class="float-left">
                <h2 class="card-title">@translate(Categories List)</h2>
            </div>
            <div class="float-right">
                <div class="row">
                    <div class="col">
                        <form method="get" action="">
                            <div class="input-group">
                                <input type="text" name="search" class="form-control col-12"
                                       placeholder="@translate(Category Name)" value="{{Request::get('search')}}">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="submit">@translate(Search)</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col">
                        <a href="#!"
                           onclick="forModal('{{ route("categories.create") }}', '@translate(Category Create)')"
                           class="btn btn-primary">
                            <i class="la la-plus"></i>
                            @translate(Add New Category)
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="card-body">
            <table class="table table-striped- table-bordered table-hover text-center">
                <thead>
                <tr>
                    <th>@translate(S/L)</th>
                    <th class="text-left">@translate(Category)</th>
                    <th class="text-left">@translate(Parent category)</th>
                    <th>@translate(Popular)</th>
                    <th>@translate(Top)</th>
                    <th>@translate(Icon class)</th>
                    <th>@translate(Image)</th>
                    <th>@translate(Publish)</th>
                    <th>@translate(Action)</th>
                </tr>
                </thead>
                <tbody>
                @forelse($categories as  $item)
                    <tr>
                        <td>{{ ($loop->index+1) + ($categories->currentPage() - 1)*$categories->perPage() }}</td>
                        <td class="text-left">{{$item->name}}</td>
                        <td  class="text-left">
                            {{$item->parent->name ?? 'N/A'}}
                        </td>
                        <td>
                            <div class="form-group">
                                <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                                    <input data-id="{{$item->id}}"
                                           {{$item->is_popular == true ? 'checked' : null}}  data-url="{{route('categories.popular')}}"
                                           type="checkbox" class="custom-control-input"
                                           id="is_popular_{{$item->id}}">
                                    <label class="custom-control-label" for="is_popular_{{$item->id}}"></label>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                                    <input data-id="{{$item->id}}"
                                           {{$item->top == true ? 'checked' : null}}  data-url="{{route('categories.top')}}"
                                           type="checkbox" class="custom-control-input"
                                           id="top_{{$item->id}}">
                                    <label class="custom-control-label" for="top_{{$item->id}}"></label>
                                </div>
                            </div>
                        </td>
                        <td class="text-center">
                            @if($item->icon != null)
                                <i class="{{$item->icon}}"></i>
                            @endif
                        </td>

                        <td>
                            @if($item->image != null)
                                <img src="{{filePath($item->image)}}" width="80" height="80"
                                     class="img-thumbnail rounded-circle" alt="{{$item->name}}">
                            @endif
                        </td>

                        <td>
                            <div class="form-group">
                                <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                                    <input data-id="{{$item->id}}"
                                           {{$item->is_published == true ? 'checked' : null}}  data-url="{{route('categories.published')}}"
                                           type="checkbox" class="custom-control-input"
                                           id="is_published_{{$item->id}}">
                                    <label class="custom-control-label" for="is_published_{{$item->id}}"></label>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-info btn-flat">@translate(Action)</button>
                                <button type="button" class="btn btn-info btn-flat dropdown-toggle"
                                        data-toggle="dropdown" aria-expanded="false">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu p-2" role="menu">
                                    <li><a  href="#!" onclick="forModal('{{ route('categories.edit', $item->id) }}', '@translate(Category Edit)')">@translate(Edit)</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#!"
                                           onclick="confirm_modal('{{ route('categories.destroy', $item->id) }}')">@translate(Delete)</a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr></tr>
                    <tr></tr>
                    <tr></tr>
                    <tr>
                        <td><h3 class="text-center">@translate(No Data Found)</h3></td>
                    </tr>
                    <tr></tr>
                    <tr></tr>
                    <tr></tr>
                    <tr></tr>
                @endforelse
                </tbody>
                <div class="float-left">
                    {{ $categories->links() }}
                </div>
            </table>
        </div>
    </div>

@endsection
