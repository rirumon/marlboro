@extends('backEnd.layouts.master')
@section('title')
    @translate(User List)
@endsection
@section('content')
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">@translate(Create User)</h3>

                <div class="float-right">
                    <div class="">
                        <a class="btn btn-success" href="{{ route("users.index") }}">
                            @translate(User List)
                        </a>
                    </div>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-2">
                <form action="{{route('users.store')}}" method="post">
                    @csrf
                    <div class="">
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">@translate(Name)</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">@translate(E-Mail Address)</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="tel" class="col-md-4 col-form-label text-md-right">@translate(Phone Number)</label>

                            <div class="col-md-6">
                                <input id="tel" type="tel" class="form-control @error('tel_number') is-invalid @enderror" name="tel_number" value="{{ old('tel_number') }}">

                                @error('tel_number')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="tel" class="col-md-4 col-form-label text-md-right">@translate(Gender)</label>

                            <div class="col-md-6">
                                <select class="form-control select2 @error('genders') is-invalid @enderror" required name="genders">
                                    <option value="">@translate(Select Option)</option>
                                    <option value="Male">@translate(Male)</option>
                                    <option value="Female">@translate(Female)</option>
                                    <option value="Other">@translate(Other)</option>
                                </select>

                                @error('genders')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">@translate(Password)</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">@translate(Confirm Password)</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">@translate(Select Groups)</label>
                            <div class="col-md-6">
                                <select class="form-control select2 w-100"  name="group_id[]"  multiple required>
                                    @foreach($groups as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                    </div>
                    <button class="btn btn-primary" type="submit">@translate(Save)</button>
                </form>
            </div>

        </div>
@endsection
