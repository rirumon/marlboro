@extends('backEnd.layouts.master')
@section('title')
    @translate(User List)
@endsection
@section('content')

    <div class="card">
        <div class="card-header">
            <h3 class="card-title">@translate(User List)</h3>

            <div class="float-right">
                <a class="btn btn-success" href="{{ route("users.create") }}">
                    @translate(Add User)
                </a>
            </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body p-2">

            <!-- there are the main content-->
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>@translate(S/L)</th>
                    <th>@translate(Avatar)</th>
                    <th>@translate(Name)</th>
                    <th>@translate(Contact)</th>
                    <th>@translate(Groups)</th>
                    <th>@translate(Last Login)</th>
                    <th>@translate(Action)</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $item)
                    <tr>
                        <td>
                            {{$loop->index+1}}
                        </td>
                        <td>
                            <img src="{{filePath($item->avatar)}}" width="80" height="80" class="img-circle">
                        </td>
                        <td>@translate(Name) : {{$item->name}} <br>
                        <strong>{{$item->gendear}}</strong>
                        </td>
                        <td> @translate(Email) : <a href="Mail:{{$item->email}}" class="text-info">{{$item->email}}</a><br>
                            @translate(Phone) : <a href="Tel:{{$item->tel_number}}" class="text-info">{{$item->tel_number}}</a>
                        <td>
                            @foreach($item->groups as $items)
                                <span class="badge badge-success">{{$items->name}}</span>,
                            @endforeach
                        </td>
                        <td>
                            @if($item->login_time != null)
                                <span class="badge badge-info">{{\Carbon\Carbon::parse($item->login_time)->diffForHumans() ?? ''}}</span>
                            @endif
                        </td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-info btn-flat">@translate(Action)</button>
                                <button type="button" class="btn btn-info btn-flat dropdown-toggle"
                                        data-toggle="dropdown" aria-expanded="false">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu p-2" role="menu">
                                    @if(\Illuminate\Support\Facades\Auth::id() == $item->id)
                                        <li><a href="{{ route('users.edit') }}">@translate(Profile)</a></li>
                                    @endif
                                    <li><a href="{{ route('users.show', $item->id) }}">@translate(Show)</a></li>
                                    @if(\Illuminate\Support\Facades\Auth::id() != $item->id)
                                        <li class="divider"></li>
                                        <li><a href="#!"
                                               onclick="confirm_modal('{{ route('users.banned',$item->id) }}')">@translate(Delete)</a>
                                        </li>
                                    @endif
                                </ul>
                            </div>

                        </td>
                    </tr>
                @endforeach

                </tbody>
                <tfoot>
                <tr>
                    <th>@translate(S/L)</th>
                    <th>@translate(Name)</th>
                    <th>@translate(Groups)</th>
                    <th>@translate(Action)</th>
                </tr>
                </tfoot>
            </table>
        </div>

    </div>


@endsection
