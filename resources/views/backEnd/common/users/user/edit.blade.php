@extends('backEnd.layouts.master')
@section('title')
    @translate(User Update)
@endsection
@section('content')

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">@translate(Update User)</h3>

                    <div class="float-right">
                        <div class="">
                            <a class="btn btn-success" href="{{ route("users.index") }}">
                                @translate(User List)
                            </a>
                        </div>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-2">
                    <form action="{{route('users.update')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id" value="{{$user->id}}"/>
                        <div class="avatar-upload">
                            <div class="avatar-edit">
                                <input type='file' name="avatar" id="imageUpload" accept=".png, .jpg, .jpeg"/>
                                <label for="imageUpload"></label>
                            </div>
                            <div class="avatar-preview">
                                <div id="imagePreview"
                                     style="background-image: url({{filePath($user->avatar)}});">
                                </div>
                            </div>
                        </div>

                        <div class="">
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">@translate(Name)</label>

                                <div class="col-md-6">
                                    <input id="name" type="text"
                                           class="form-control @error('name') is-invalid @enderror" name="name"
                                           value="{{ $user->name}}" required autocomplete="name" autofocus>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">@translate(E-Mail
                                    Address)</label>

                                <div class="col-md-6">
                                    <input id="email" type="email"
                                           class="form-control @error('email') is-invalid @enderror" name="email"
                                           value="{{ $user->email}}" required autocomplete="email">

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">@translate(Phone number)</label>

                                <div class="col-md-6">
                                    <input id="name" type="tel"
                                           class="form-control @error('tel_number') is-invalid @enderror"  name="tel_number"
                                           value="{{ $user->tel_number}}"  autocomplete="name" autofocus>

                                    @error('tel_number')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">@translate(Select Gender)</label>
                                <div class="col-md-6">
                                    <select class="form-control select2 w-100 @error('genders') is-invalid @enderror" required name="genders">
                                        <option value=""></option>
                                        <option value="Male" {{$user->genders == "Male" ? 'selected': null}}>
                                            @translate(Male)
                                        </option>
                                        <option value="Female" {{$user->genders == "Female" ? 'selected': null}}>
                                            @translate(Female)
                                        </option>
                                        <option value="Other" {{$user->genders == "Other" ? 'selected': null}}>
                                            @translate(Other)
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">@translate(Select Groups)</label>
                                <div class="col-md-6">
                                    <select class="form-control select2 w-100" name="group_id[]" multiple required>
                                        @foreach($groups as $item)
                                            <option value="{{$item->id}}"
                                            @foreach($user->groups as $item1)
                                                {{$item1->id == $item->id ? 'selected' : null}}
                                                @endforeach
                                            >{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-primary m-2" type="submit">@translate(Update)</button>
                    </form>
                </div>

            </div>

@endsection
