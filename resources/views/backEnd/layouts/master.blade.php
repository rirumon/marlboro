<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>{{env('APP_NAME')}} | @yield('title')</title>
    <link rel="icon" href="{{filePath(getSystemSetting('favicon_icon'))}}">

    @include('backEnd.layouts.includes.style')


</head>

<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
    @include('backEnd.layouts.includes.navbar')
    @include('backEnd.layouts.includes.aside')

    <div class="content-wrapper">
        <div class="content-header">
            @include('backEnd.layouts.includes.breadcrumb')
                        @include('backEnd.layouts.includes.error')
        </div>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                @yield('content')
            </div>
            <!--/. container-fluid -->
        </section>
    </div>
    @include('backEnd.layouts.includes.footer')
    @include('backEnd.layouts.includes.model')
    @include('backEnd.layouts.includes.delete')
</div>
@include('backEnd.layouts.includes.script')
@include('sweetalert::alert')
</body>

</html>
