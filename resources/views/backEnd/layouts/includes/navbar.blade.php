<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav float-left">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="{{route('dashboard')}}" class="nav-link">@translate(Home)</a>
        </li>
    </ul>


    <ul class="navbar-nav ml-auto">

        {{--currency--}}
        <li class="dropdown mx-2">
            <div class="m-2">
                <a class="dropdown-toggle text-dark" href="#" role="button" id="languagelink"
                   data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false"><span class="live-icon">
                                        {{Str::ucfirst(defaultCurrency())}}
                                    </span><span class="feather icon-chevron-down live-icon"></span>
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" aria-labelledby="languagelink">
                    @foreach(\App\Models\Currency::where('is_published',true)->get() as $item)
                        <a class="dropdown-item" href="{{route('currencies.change')}}" onclick="event.preventDefault();
                            document.getElementById('{{$item->name}}').submit()">
                            <img width="25" height="auto" src="{{ asset("images/lang/". $item->image) }}" alt=""/>
                            {{$item->name}}</a>
                        <form id="{{$item->name}}" class="d-none" action="{{ route('currencies.change') }}"
                              method="POST">
                            @csrf
                            <input type="hidden" name="code" value="{{$item->id}}">
                        </form>
                    @endforeach
                </div>
            </div>

        </li>

        {{--languages--}}
        <li class="dropdown mx-2">
            <div class="m-2">
                <a class="dropdown-toggle text-dark" href="#" role="button" id="languagelink"
                   data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false"><span class="live-icon">
                                        {{Str::ucfirst(\Illuminate\Support\Facades\Session::get('locale') ?? env('DEFAULT_LANGUAGE'))}}
                                    </span><span class="feather icon-chevron-down live-icon"></span>
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" aria-labelledby="languagelink">
                    @foreach(\App\Models\Language::all() as $language)
                        <a class="dropdown-item" href="{{route('language.change')}}" onclick="event.preventDefault();
                            document.getElementById('{{$language->name}}').submit()">
                            <img width="25" height="auto" src="{{ asset("images/lang/". $language->image) }}" alt=""/>
                            {{$language->name}}</a>
                        <form id="{{$language->name}}" class="d-none" action="{{ route('language.change') }}"
                              method="POST">
                            @csrf
                            <input type="hidden" name="code" value="{{$language->code}}">
                        </form>
                    @endforeach
                </div>
            </div>

        </li>

        <!-- Messages Dropdown Menu -->
        <li class="dropdown  mx-2">
            <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="far fa-comments"></i>
                <span class="badge badge-danger navbar-badge">3</span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <a href="#" class="dropdown-item">
                    <!-- Message Start -->
                    <div class="media">
                        <img src="{{filePath(\Illuminate\Support\Facades\Auth::user()->avatar)}}"
                             alt="{{\Illuminate\Support\Facades\Auth::user()->name}}"
                             class="img-size-50 mr-3 img-circle">
                        <div class="media-body">
                            <h3 class="dropdown-item-title">
                                Brad Diesel
                                <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                            </h3>
                            <p class="text-sm">Call me whenever you can...</p>
                            <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                        </div>
                    </div>
                    <!-- Message End -->
                </a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item">
                    <!-- Message Start -->
                    <div class="media">
                        <img src="{{filePath(\Illuminate\Support\Facades\Auth::user()->avatar)}}"
                             alt="{{\Illuminate\Support\Facades\Auth::user()->name}}"
                             class="img-size-50 img-circle mr-3">
                        <div class="media-body">
                            <h3 class="dropdown-item-title">
                                John Pierce
                                <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
                            </h3>
                            <p class="text-sm">I got your message bro</p>
                            <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                        </div>
                    </div>
                    <!-- Message End -->
                </a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item">
                    <!-- Message Start -->
                    <div class="media">
                        <img src="{{filePath(\Illuminate\Support\Facades\Auth::user()->avatar)}}"
                             alt="{{\Illuminate\Support\Facades\Auth::user()->name}}"
                             class="img-size-50 img-circle mr-3">
                        <div class="media-body">
                            <h3 class="dropdown-item-title">
                                Nora Silvester
                                <span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>
                            </h3>
                            <p class="text-sm">The subject goes here</p>
                            <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                        </div>
                    </div>
                    <!-- Message End -->
                </a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
            </div>
        </li>
        <!-- Notifications Dropdown Menu -->
        <li class="dropdown  m-l-2">
            <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="far fa-bell"></i>
                <span class="badge badge-warning navbar-badge">15</span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <span class="dropdown-item dropdown-header">15 Notifications</span>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item">
                    <i class="fas fa-envelope mr-2"></i> 4 new messages
                    <span class="float-right text-muted text-sm">3 mins</span>
                </a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item">
                    <i class="fas fa-users mr-2"></i> 8 friend requests
                    <span class="float-right text-muted text-sm">12 hours</span>
                </a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item">
                    <i class="fas fa-file mr-2"></i> 3 new reports
                    <span class="float-right text-muted text-sm">2 days</span>
                </a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
            </div>
        </li>

        <li class="dropdown user user-menu  mx-2">
            <a class="dropdown-toggle" href="#" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
               aria-expanded="false">
                <img src="{{filePath(\Illuminate\Support\Facades\Auth::user()->avatar)}}"
                     class="img-circle m-b-1" width="40px" height="40px"
                     alt="{{filePath(\Illuminate\Support\Facades\Auth::user()->name)}}">
            </a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="{{route('users.show',\Illuminate\Support\Facades\Auth::id())}}">@translate(Profile)</a>
                <a class="dropdown-divider"></a>
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    @translate(Logout)
                </a>

                <form id="logout-form" class="d-none" action="{{ route('logout') }}" method="POST">
                    @csrf
                </form>

                <a class="dropdown-item" href="#">Something else here</a>
            </div>

        </li>

        <!--Setting-->
        <li class="nav-item">
            <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
                <i class="fas fa-th-large"></i>
            </a>
        </li>

    </ul>
</nav>
