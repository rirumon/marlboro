<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    //for recursive menu
    public function childrenCategories()
    {
        return $this->hasMany(Category::class, 'parent_category_id', 'id')->with('childrenCategories');
    }

    public function scopePublished($query)
    {
        return $query->where('is_published', 1);
    }


    public function parent()
    {
        return $this->hasOne(Category::class, 'id', 'parent_category_id');
    }
}
